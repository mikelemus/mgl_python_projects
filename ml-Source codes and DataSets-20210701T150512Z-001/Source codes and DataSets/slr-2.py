import matplotlib.pyplot as plt
from scipy import stats

advertising = [23,26,30,34,43,48,52,57,58]
sales = [651,762,856,1063,1190,1298,1421,1440,1518]

beta1, beta0, r_value, p_value, std_err = stats.linregress(advertising, sales)

def predict(advertising):
  return beta0 + beta1 * advertising   

mymodel = list(map(predict, advertising))

predicted_sales = predict(54)

print("advertising data", advertising)
print("sales data", sales)
print("When investing", 54, "mill in advertising, predicted sales = ",  predicted_sales)

plt.scatter(advertising, sales)
plt.plot(advertising, mymodel)
plt.show()


