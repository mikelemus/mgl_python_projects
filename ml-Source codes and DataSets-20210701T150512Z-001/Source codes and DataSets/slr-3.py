import matplotlib.pyplot as plt
from scipy import stats

months = [1,3,4,6,8,9,11,12,14]
sales = [651,762,856,1063,1190,1298,1421,1440,1518]

beta1, beta0, r_value, p_value, std_err = stats.linregress(months, sales)

def predict(months):
  return beta0 + beta1 * months   

mymodel = list(map(predict, months))

predicted_sales = predict(16)

print("Months data: ", months)
print("Sales data: ", sales)
print("Predicted sales = ",  predicted_sales)
print("Predicted Sales for month", 16, "are = ", predicted_sales)

plt.scatter(months, sales)
plt.plot(months, mymodel)
plt.show()


