import scrapy
 
 
class SimplifiedSpider(scrapy.Spider):
    name = "simplified"
 
    def start_requests(self):
        urls = [
            'https://www.simplifiedpython.net/page/2/',
            'https://www.simplifiedpython.net/page/3/', 
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)
 
    def parse(self, response):
        page = response.url.split("/")[-2]
        filename = 'page-%s.html' % page
        with open(filename, 'wb') as f:
            f.write(response.body)
        self.log('Saved file %s' % filename)
